import { plainToInstance } from 'class-transformer';
import { ENVTypes } from './constants';

class ProcessENV {
  public CUSTOM_ENV: ENVTypes = ENVTypes.DEV;

  //Порты для запуска самого сервера
  public SERVER_PORT: number = 3000;

  //URL
  public SERVER_URL: string = '';

  //Всё что касается основной БД
  public DB_URL_1: string = '195.2.85.173';
  public DB_PORT_1: number = 5100;
  public DB_NAME_1: string = 'postgres';
  public DB_USERNAME_1: string = 'postgres';
  public DB_PASSWORD_1: string = 'custompassword';

  public DB_URL_2: string = '195.2.85.173';
  public DB_PORT_2: number = 5200;
  public DB_NAME_2: string = 'postgres';
  public DB_USERNAME_2: string = 'postgres';
  public DB_PASSWORD_2: string = 'custompassword';

  //Все что касается редиски
  public REDIS_DB_URL: string = 'IP';
  public REDIS_PORT: number = 6379;
  public REDIS_DB_NUMBER: number = 0;
  public REDIS_PASSWORD: string = '';
}

export default class UtilsENVConfig {
  private static processENV: ProcessENV | null = null;

  static getProcessEnv(): ProcessENV {
    if (this.processENV === null) {
      this.processENV = plainToInstance(ProcessENV, process.env);
    }
    return this.processENV;
  }

  static isAvailable(...availableTypes: ENVTypes[]): boolean {
    return availableTypes.includes(this.getProcessEnv().CUSTOM_ENV);
  }
}
