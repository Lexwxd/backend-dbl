import { Client } from 'pg';
import { Sequelize } from 'sequelize-typescript';
import UtilsENVConfig from 'utils/utils-env-config';
import Log from 'utils/utils-log';
import { config1, config2 } from './config';
import SequelizeMulti from './multi';

export async function createDbIfNotExist() {
  const dbName = UtilsENVConfig.getProcessEnv().DB_NAME_1;
  const client = new Client({
    user: UtilsENVConfig.getProcessEnv().DB_USERNAME_1,
    password: UtilsENVConfig.getProcessEnv().DB_PASSWORD_1,
    database: 'postgres',
    host: UtilsENVConfig.getProcessEnv().DB_URL_1,
    port: UtilsENVConfig.getProcessEnv().DB_PORT_1,
  });

  await client.connect();

  const allDB = await client.query('SELECT datname FROM pg_database');

  if (allDB.rows.findIndex((el) => el.datname === dbName.toLowerCase()) === -1) {
    Log.info('creating database');
    await client.query(`CREATE DATABASE ${dbName}`);
  }
  await client.end();
}

export async function initSequelize() {
  try {

    // await sequelizeInstance.dropSchema('public', { });
    // await sequelizeInstance.createSchema('public', {  });
    //await sequelizeInstance1.sync();
    
    const sequelizeInstance2 = new Sequelize(config2);
    await sequelizeInstance2.authenticate();
    const sequelizeInstance1 = new Sequelize(config1);
    await sequelizeInstance1.authenticate();
    // await sequelizeInstance.dropSchema('public', { });
    // await sequelizeInstance.createSchema('public', {  });
    //await sequelizeInstance2.sync();
    SequelizeMulti.instance1 = sequelizeInstance1;
    SequelizeMulti.instance2 = sequelizeInstance2;
    Log.info('Sequelize was initialized');
  } catch (error) {
    Log.error('Sequelize ERROR', error);
    process.exit();
  }
}

export async function dropDB() {
  try {
    await SequelizeMulti.getInstance().instance.dropSchema('public', {});
    await SequelizeMulti.getInstance().instance.createSchema('public', {});
    await SequelizeMulti.getInstance().instance.sync();
    Log.info('Sequelize was initialized');
  } catch (error) {
    Log.error('Sequelize DROP_DB ERROR', error);
  }
}
