import { BelongsTo, Column, DataType, ForeignKey, Model, Table } from 'sequelize-typescript';
import Catalog from './catalog.model';

@Table({
  timestamps: true,
})
export default class Item extends Model {
  @Column({
    defaultValue: DataType.UUIDV4,
    type: DataType.UUID,
    primaryKey: true,
  })
  public id!: string;

  @Column({
    allowNull: false
  })
  public title!: string;

  @ForeignKey(() => Catalog)
  public catalogId!: string;

  @BelongsTo(() => Catalog, 'catalogId')
  public catalog!: Catalog;

}
