import {
  Attributes,
  ConnectionError,
  CreateOptions,
  CreationAttributes,
  FindOptions,
  Identifier,
  InstanceUpdateOptions,
  UpdateOptions,
  DestroyOptions,
} from 'sequelize';
import { Model, ModelCtor, Sequelize } from 'sequelize-typescript';
import { Col, Fn, Literal } from 'sequelize/types/utils';
import Log from 'utils/utils-log';

export default class SequelizeMulti {
  static instance1: Sequelize;
  static instance2: Sequelize;
  static current: number = 1;
  static workInstanceNumber = 0;

  static getInstanceByNumber(n: number) {
    if (n === 1) {
      return this.instance1;
    } else {
      return this.instance2;
    }
  }

  static getInstance() {
    console.log(this.workInstanceNumber)
    if (this.workInstanceNumber === 1) {
      return { instance: this.instance1, number: null };
    } else if (this.workInstanceNumber === 2) {
      return { instance: this.instance2, number: null };
    }
    if (this.current === 1) {
      this.current = 2;
      console.log('1111111111111111111111111111111111111111111111111111111111\n\n\n')
      return { instance: this.instance1, number: 1 };
    }
    console.log('222222222222222222222222222222222222222222222222222222222111\n\n\n')
    this.current = 1;
    return { instance: this.instance2, number: 2 };
  }

  static findAll = async <T extends Model<any, any>>(model: ModelCtor<T>, options?: FindOptions<Attributes<T>>) => {
    let result;
    const { instance: sequelize, number } = SequelizeMulti.getInstance();
    try {
      result = await (sequelize.models[model.name] as ModelCtor<T>).findAll<T>(options);
    } catch (error) {
      if (error instanceof ConnectionError) {
        console.log('NUMBERRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR: ', number)
        if (!number) {
          Log.info('All database error1');
          console.log(error)
          process.exit(1);
        }
        SequelizeMulti.workInstanceNumber = number === 1 ? 2 : 1;
        try {
          result = await (SequelizeMulti.getInstance().instance.models[model.name] as ModelCtor<T>).findAll<T>(options);
        } catch (error) {
          if (error instanceof ConnectionError) {
            Log.info('All database error2');
            console.log(error)
            process.exit(1);
          } else {
            throw error;
          }
        }
      } else {
        throw error;
      }
    }
    return result;
  };

  static findByPk = async <T extends Model<any, any>>(
    model: ModelCtor<T>,
    identifier?: Identifier,
    options?: Omit<FindOptions<Attributes<T>>, 'where'>
  ) => {
    let result;
    const { instance: sequelize, number } = SequelizeMulti.getInstance();
    try {
      result = await (sequelize.models[model.name] as ModelCtor<T>).findByPk<T>(identifier, options);
    } catch (error) {
      if (error instanceof ConnectionError) {
        if (!number) {
          Log.info('All database error');
          process.exit(1);
        }
        SequelizeMulti.workInstanceNumber = number === 1 ? 2 : 1;
        try {
          result = await (SequelizeMulti.getInstance().instance.models[model.name] as ModelCtor<T>).findByPk<T>(
            identifier,
            options
          );
        } catch (error) {
          if (error instanceof ConnectionError) {
            Log.info('All database error');
            process.exit(1);
          } else {
            throw error;
          }
        }
      } else {
        throw error;
      }
    }
    return result;
  };

  static create = async <
    T extends Model<any, any>,
    O extends CreateOptions<Attributes<T>> = CreateOptions<Attributes<T>>
  >(
    model: ModelCtor<T>,
    values?: CreationAttributes<T>,
    options?: O
  ) => {
    let result;
    const { instance: sequelize, number } = SequelizeMulti.getInstance();
    try {
      result = await (sequelize.models[model.name] as ModelCtor<T>).create<T>(values, options);
    } catch (error) {
      if (error instanceof ConnectionError) {
        if (!number) {
          Log.info('All database error');
          process.exit(1);
        }
        SequelizeMulti.workInstanceNumber = number === 1 ? 2 : 1;
        try {
          result = await (SequelizeMulti.getInstance().instance.models[model.name] as ModelCtor<T>).create<T>(
            values,
            options
          );
        } catch (error) {
          if (error instanceof ConnectionError) {
            Log.info('All database error');
            process.exit(1);
          } else {
            throw error;
          }
        }
      } else {
        throw error;
      }
    }
    return result;
  };

  static update = async <T extends Model<any, any>>(
    model: ModelCtor<T>,
    values: any,
    options: UpdateOptions<Attributes<T>>
  ) => {
    let result;
    const { instance: sequelize, number } = SequelizeMulti.getInstance();
    try {
      result = ((await (sequelize.models[model.name] as ModelCtor<any>)) as any).update(values, options);
    } catch (error) {
      if (error instanceof ConnectionError) {
        if (!number) {
          Log.info('All database error');
          process.exit(1);
        }
        SequelizeMulti.workInstanceNumber = number === 1 ? 2 : 1;
        try {
          result = await (SequelizeMulti.getInstance().instance.models[model.name] as ModelCtor<any> as any).update(
            values,
            options
          );
        } catch (error) {
          if (error instanceof ConnectionError) {
            Log.info('All database error');
            process.exit(1);
          } else {
            throw error;
          }
        }
      } else {
        throw error;
      }
    }
    return result;
  };

  static destroy = async <T extends Model<any, any>>(
    model: ModelCtor<T>,
    options?: DestroyOptions<Attributes<T>>
  ) => {
    let result;
    const { instance: sequelize, number } = SequelizeMulti.getInstance();
    try {
      result = ((await (sequelize.models[model.name] as ModelCtor<any>)) as any).destroy(options);
    } catch (error) {
      if (error instanceof ConnectionError) {
        if (!number) {
          Log.info('All database error');
          process.exit(1);
        }
        SequelizeMulti.workInstanceNumber = number === 1 ? 2 : 1;
        try {
          result = await (SequelizeMulti.getInstance().instance.models[model.name] as ModelCtor<any> as any).destroy(
            options
          );
        } catch (error) {
          if (error instanceof ConnectionError) {
            Log.info('All database error');
            process.exit(1);
          } else {
            throw error;
          }
        }
      } else {
        throw error;
      }
    }
    return result;
  }
}
