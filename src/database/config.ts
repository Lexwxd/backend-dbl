import { SequelizeOptions } from 'sequelize-typescript';
import UtilsENVConfig from 'utils/utils-env-config';

export const config1: SequelizeOptions = {
  host: UtilsENVConfig.getProcessEnv().DB_URL_1,
  port: UtilsENVConfig.getProcessEnv().DB_PORT_1,
  database: UtilsENVConfig.getProcessEnv().DB_NAME_1,
  dialect: 'postgres',
  username: UtilsENVConfig.getProcessEnv().DB_USERNAME_1,
  password: UtilsENVConfig.getProcessEnv().DB_PASSWORD_1,
  models: [__dirname + '/models/final/*.model.*', __dirname + '/models/relations/*.model.*'],
  pool: {
    max: 3,
    min: 0,
    acquire: 30000,
    idle: 10000,
  },
};

export const config2: SequelizeOptions = {
  host: UtilsENVConfig.getProcessEnv().DB_URL_2,
  port: UtilsENVConfig.getProcessEnv().DB_PORT_2,
  database: UtilsENVConfig.getProcessEnv().DB_NAME_2,
  dialect: 'postgres',
  username: UtilsENVConfig.getProcessEnv().DB_USERNAME_2,
  password: UtilsENVConfig.getProcessEnv().DB_PASSWORD_2,
  models: [__dirname + '/models2/final/*.model.*', __dirname + '/models2/relations/*.model.*'],
  pool: {
    max: 3,
    min: 0,
    acquire: 30000,
    idle: 10000,
  },
}
