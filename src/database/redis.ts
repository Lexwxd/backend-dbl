import * as Redis from 'redis';
import UtilsENVConfig from 'utils/utils-env-config';
import Log from 'utils/utils-log';

export const redis = Redis.createClient({
  url:
    'redis://' +
    UtilsENVConfig.getProcessEnv().REDIS_DB_URL +
    ':' +
    UtilsENVConfig.getProcessEnv().REDIS_PORT +
    '/' +
    UtilsENVConfig.getProcessEnv().REDIS_DB_NUMBER,
  password: UtilsENVConfig.getProcessEnv().REDIS_PASSWORD,
});

export async function initRedis() {
  redis.on('error', (err: any) => {
    Log.error('Redis ERROR', err);
    process.exit();
  });

  await redis.connect();

  // await redis.flushDb();

  Log.info('Redis was initialized');
}
