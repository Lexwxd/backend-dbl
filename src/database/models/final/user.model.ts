import { BelongsTo, Column, DataType, ForeignKey, HasMany, Model, Table } from 'sequelize-typescript';
import Catalog from './catalog.model';
import Order from './order.model';

@Table({
  timestamps: true,
})
export default class User extends Model {
  @Column({
    defaultValue: DataType.UUIDV4,
    type: DataType.UUID,
    primaryKey: true,
  })
  public id!: string;

  @Column({
    allowNull: false
  })
  public phone!: string;

  @Column({
    allowNull: false
  })
  public name!: string;

  @HasMany(() => Order)
  public orderList!: Order[];

}