import { Column, DataType, HasMany, Model, Table } from 'sequelize-typescript';
import Item from './item.model';

@Table({
  timestamps: true,
})
export default class Catalog extends Model {
  @Column({
    defaultValue: DataType.UUIDV4,
    type: DataType.UUID,
    primaryKey: true,
  })
  public id!: string;

  @Column({
    allowNull: false
  })
  public title!: string;

  @HasMany(() => Item)
  public itemList!: Item[];

}