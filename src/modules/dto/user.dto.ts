import { IsString } from 'class-validator';
import { BaseDto } from 'modules/base/base.dto';


export class UserDto extends BaseDto {

  @IsString()
  readonly phone!: string;

  @IsString()
  readonly name!: string;

}
