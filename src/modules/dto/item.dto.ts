import { IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { BaseDto } from 'modules/base/base.dto';


export class ItemDto extends BaseDto {

  @IsOptional()
  catalogId!: string

  @IsString()
  // @IsOptional()
  @IsNotEmpty()
  title!: string;

}
