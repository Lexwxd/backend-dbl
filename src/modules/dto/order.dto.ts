import { IsNotEmpty, IsArray, IsString, IsOptional } from 'class-validator';
import { BaseDto } from 'modules/base/base.dto';


export class OrderDto extends BaseDto {

  @IsArray()
  // @IsOptional()
  basket!: object[];

  @IsString()
  @IsOptional()
  @IsNotEmpty()
  userId!: string;

}
