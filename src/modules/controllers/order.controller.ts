import { ApiController, DELETE, GET, PATCH, POST } from 'core/api-decorators';
import { NextFunction, Response } from 'express';
import { dtoValidator } from 'middlewares/validate';
import BaseRequest from 'modules/base/base.request';
import { OrderDto } from 'modules/dto/order.dto';
import OrdersService from 'modules/services/order.service';
import { throwErrorNotFound } from 'utils/utils-errors';

@ApiController('/order')
class ExampleController {
  @GET('/')
  async getTemplate(req: BaseRequest, res: Response, next: NextFunction) {
    const userId = req.header('x-user')
    if(!userId){
      throwErrorNotFound('user is not found')
    }
    const orders = await OrdersService.getOrders(userId);
    res.json(orders);
  }

  @GET('/:id')
  async getTemplateById(req: BaseRequest, res: Response, next: NextFunction) {
    const id = req.params.id;
    const order = await OrdersService.getOrderById(id);
    res.json(order);
  }

  @POST('/', {
    handlers: [dtoValidator(OrderDto)],
  })
  async createTemplate(req: BaseRequest, res: Response, next: NextFunction) {
    const dto: OrderDto = req.body;
    const userId = req.header('x-user')
    if(!userId){
      throwErrorNotFound('user is not found')
    }
    dto.userId = userId
    const item = await OrdersService.createOrder(dto);
    res.json(item);
  }

  @PATCH('/:id', {
    handlers: [dtoValidator(OrderDto)],
  })
  async updateTemplate(req: BaseRequest, res: Response, next: NextFunction) {
    const id = req.params.id;
    const dto: OrderDto = req.body;
    const userId = req.header('x-user')
    if(!userId){
      throwErrorNotFound('user is not found')
    }
    dto.userId = userId
    const item = await OrdersService.updateOrder(id, dto);
    res.json(item);
  }

  @DELETE('/:id')
  async deleteTemplate(req: BaseRequest, res: Response, next: NextFunction) {
    const id = req.params.id;
    const result = await OrdersService.deleteOrder(id);
    res.json(result);
  }
}

export default new ExampleController();
