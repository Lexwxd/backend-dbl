import { ApiController, DELETE, GET, PATCH, POST } from 'core/api-decorators';
import { NextFunction, Response } from 'express';
import { dtoValidator } from 'middlewares/validate';
import BaseRequest from 'modules/base/base.request';
import { CatalogDto } from 'modules/dto/catalog.dto';
import CatalogService from 'modules/services/catalog.service';

@ApiController('/catalog')
class ExampleController {
  @GET('/')
  async getTemplate(req: BaseRequest, res: Response, next: NextFunction) {
    const items = await CatalogService.getItems();
    res.json(items);
  }

  @GET('/:id')
  async getTemplateById(req: BaseRequest, res: Response, next: NextFunction) {
    const id = req.params.id;
    const item = await CatalogService.getItemById(id);
    res.json(item);
  }

  @POST('/', {
    handlers: [dtoValidator(CatalogDto)],
  })
  async createTemplate(req: BaseRequest, res: Response, next: NextFunction) {
    const dto: CatalogDto = req.body;
    const item = await CatalogService.createItem(dto);
    res.json(item);
  }

  @PATCH('/:id', {
    handlers: [dtoValidator(CatalogDto)],
  })
  async updateTemplate(req: BaseRequest, res: Response, next: NextFunction) {
    const id = req.params.id;
    const dto: CatalogDto = req.body;
    const item = await CatalogService.updateItem(id, dto);
    res.json(item);
  }

  @DELETE('/:id')
  async deleteTemplate(req: BaseRequest, res: Response, next: NextFunction) {
    const id = req.params.id;
    const result = await CatalogService.deleteItem(id);
    res.json(result);
  }
}

export default new ExampleController();
