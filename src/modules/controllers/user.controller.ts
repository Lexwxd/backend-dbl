import { ApiController, DELETE, GET, PATCH, POST } from 'core/api-decorators';
import { NextFunction, Response } from 'express';
import { dtoValidator } from 'middlewares/validate';
import BaseRequest from 'modules/base/base.request';
import { UserDto } from 'modules/dto/user.dto';
import UsersService from 'modules/services/user.service';

@ApiController('/user')
class ExampleController {
  @GET('/')
  async getTemplate(req: BaseRequest, res: Response, next: NextFunction) {
    const items = await UsersService.getItems();
    res.json(items);
  }

  @GET('/:id')
  async getTemplateById(req: BaseRequest, res: Response, next: NextFunction) {
    const id = req.params.id;
    const item = await UsersService.getItemById(id);
    res.json(item);
  }

  @POST('/', {
    handlers: [dtoValidator(UserDto)],
  })
  async createTemplate(req: BaseRequest, res: Response, next: NextFunction) {
    const dto: UserDto = req.body;
    const item = await UsersService.createItem(dto);
    res.json(item);
  }

  @PATCH('/:id', {
    handlers: [dtoValidator(UserDto)],
  })
  async updateTemplate(req: BaseRequest, res: Response, next: NextFunction) {
    const id = req.params.id;
    const dto: UserDto = req.body;
    const item = await UsersService.updateItem(id, dto);
    res.json(item);
  }

  @DELETE('/:id')
  async deleteTemplate(req: BaseRequest, res: Response, next: NextFunction) {
    const id = req.params.id;
    const result = await UsersService.deleteItem(id);
    res.json(result);
  }
}

export default new ExampleController();
