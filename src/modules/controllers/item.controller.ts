import { ApiController, DELETE, GET, PATCH, POST } from 'core/api-decorators';
import { NextFunction, Response } from 'express';
import { dtoValidator } from 'middlewares/validate';
import BaseRequest from 'modules/base/base.request';
import { ItemDto } from 'modules/dto/item.dto';
import ItemsService from 'modules/services/item.service';

@ApiController('/item')
class ExampleController {
  @GET('/')
  async getTemplate(req: BaseRequest, res: Response, next: NextFunction) {
    const items = await ItemsService.getItems();
    res.json(items);
  }

  @GET('/:id')
  async getTemplateById(req: BaseRequest, res: Response, next: NextFunction) {
    const id = req.params.id;
    const item = await ItemsService.getItemById(id);
    res.json(item);
  }

  @POST('/', {
    handlers: [dtoValidator(ItemDto)],
  })
  async createTemplate(req: BaseRequest, res: Response, next: NextFunction) {
    const dto: ItemDto = req.body;
    const item = await ItemsService.createItem(dto);
    res.json(item);
  }

  @PATCH('/:id', {
    handlers: [dtoValidator(ItemDto)],
  })
  async updateTemplate(req: BaseRequest, res: Response, next: NextFunction) {
    const id = req.params.id;
    const dto: ItemDto = req.body;
    const item = await ItemsService.updateItem(id, dto);
    res.json(item);
  }

  @DELETE('/:id')
  async deleteTemplate(req: BaseRequest, res: Response, next: NextFunction) {
    const id = req.params.id;
    const result = await ItemsService.deleteItem(id);
    res.json(result);
  }
}

export default new ExampleController();
