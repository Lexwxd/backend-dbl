import Item from 'database/models/final/item.model';
import SequelizeMulti from 'database/multi';
import { ItemDto } from 'modules/dto/item.dto';
import { throwErrorNotFound } from 'utils/utils-errors';

export default class ItemsService {
  static async getItems() {
    return await SequelizeMulti.findAll(Item);
  }

  static async getItemById(id: string) {
    const item = await SequelizeMulti.findByPk(Item, id);

    if (!item) {
      throwErrorNotFound(`Item with id# ${id} was not found`);
    }

    return item;
  }

  static async createItem(dto: ItemDto) {
    const item = await SequelizeMulti.create(Item, { ...dto });

    return item;
  }

  static async updateItem(id: string, dto: ItemDto) {
    await SequelizeMulti.update(Item, dto, { where: { id } });
    
    const item = await this.getItemById(id);

    return item;
  }

  static async deleteItem(id: string) {
    await SequelizeMulti.destroy(Item, { where: { id } });
  }
}
