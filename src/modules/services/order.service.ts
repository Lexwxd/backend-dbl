import Order from 'database/models/final/order.model';
import SequelizeMulti from 'database/multi';
import { OrderDto } from 'modules/dto/order.dto';
import { throwErrorNotFound } from 'utils/utils-errors';

export default class OrdersService {
  static async getOrders(userId: string) {
    return await Order.findAll({where: {
      userId
    }});
  }

  static async getOrderById(id: string) {
    const order = await SequelizeMulti.findByPk(Order, id);

    if (!order) {
      throwErrorNotFound(`Order with id# ${id} was not found`);
    }

    return order;
  }

  static async createOrder(dto: OrderDto) {
    const order = await SequelizeMulti.create(Order, { ...dto});

    return order;
  }

  static async updateOrder(id: string, dto: OrderDto) {
    await SequelizeMulti.update(Order, dto, { where: { id } });
    
    const order = await this.getOrderById(id);

    return order;
  }

  static async deleteOrder(id: string) {
    await SequelizeMulti.destroy(Order, { where: { id } });
  }
}
