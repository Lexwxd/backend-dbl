import User from 'database/models/final/user.model';
import SequelizeMulti from 'database/multi';
import { UserDto } from 'modules/dto/user.dto';
import { throwErrorNotFound } from 'utils/utils-errors';

export default class UsersService {
  static async getItems() {
    return await SequelizeMulti.findAll(User);
  }

  static async getItemById(id: string) {
    const item = await SequelizeMulti.findByPk(User, id);

    if (!item) {
      throwErrorNotFound(`User with id# ${id} was not found`);
    }

    return item;
  }

  static async createItem(dto: UserDto) {
    const item = await SequelizeMulti.create(User, { ...dto });

    return item;
  }

  static async updateItem(id: string, dto: UserDto) {
    await SequelizeMulti.update(User, dto, { where: { id } });
    
    const item = await this.getItemById(id);

    return item;
  }

  static async deleteItem(id: string) {
    await SequelizeMulti.destroy(User, { where: { id } });
  }
}
