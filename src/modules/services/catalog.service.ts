import Catalog from 'database/models/final/catalog.model';
import Item from 'database/models/final/item.model';
import SequelizeMulti from 'database/multi';
import { CatalogDto } from 'modules/dto/catalog.dto';
import { throwErrorNotFound } from 'utils/utils-errors';

export default class CatalogService {
  static async getItems() {
    return await SequelizeMulti.findAll(Catalog, { include: ['itemList'] });
  }

  static async getItemById(id: string) {
    const item = await SequelizeMulti.findByPk(Catalog, id);

    if (!item) {
      throwErrorNotFound(`Catalogs with id# ${id} was not found`);
    }

    return item;
  }

  static async createItem(dto: CatalogDto) {
    const item = await SequelizeMulti.create(Catalog, { ...dto });

    return item;
  }

  static async updateItem(id: string, dto: CatalogDto) {
    await SequelizeMulti.update(Catalog, dto, { where: { id } });
    
    const item = await this.getItemById(id);
    
    return item;
  }

  static async deleteItem(id: string) {
    await SequelizeMulti.destroy(Catalog, { where: { id } });
  }
}
